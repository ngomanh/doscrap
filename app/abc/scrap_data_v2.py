import bs4 as bs
import urllib.request as request
import ssl
from models import Scrap
from database import session
import time

gcontext = ssl.SSLContext()


class Tools:
    def insert(self, data):
        try:
            s = self.parser_scrap(self=None, data=data, s=None)
            session.add(s)
            session.commit()
            print('add successfully !')
        except Exception as e:
            print(e.__str__())
            session.rollback()
        finally:
            session.close()

        # for elem in elems:
        #     print(elem.get_attribute("title"))
        #     print(elem.text)
        return data

    def parser_scrap(self, data, s):
        title, content = None, None
        if 'title' in data:
            title = str(data['title'])
        if 'content' in data:
            content = str(data['content'])
        if s is None:
            s = Scrap(title=title, content=content)
        else:
            s.tittle = title,
            s.content = content

        return s


class ScrapData(object):
    def main_data():
        sauce = request.urlopen(f'https://vnexpress.net/giao-duc', context=gcontext).read()
        soup_max = bs.BeautifulSoup(sauce, 'lxml')

        for i in soup_max.find_all('input'):
            find_max = i.get('max-page')
            if type(find_max) is str:
                max = int(find_max)

        while True:
            try:
                for i in range(max):
                    sauce_page = request.urlopen(f'https://vnexpress.net/giao-duc-p{i}', context=gcontext).read()
                break
            except:
                time.sleep(2)

        soup_page = bs.BeautifulSoup(sauce_page, 'lxml')

        for a_all in soup_page.find_all('a'):
            tit_con = {'title': a_all.get('title'), 'content': a_all.text}
            if "\n" not in str(a_all.text):
                Tools.insert(self=Tools, data=tit_con)


if __name__ == '__main__':
    ScrapData.main_data()
