from selenium import webdriver
from models import Scrap
from app import session

driver = webdriver.Chrome('chromedriver')  # enter the path
driver.get("https://vnexpress.net/giao-duc")
elems = driver.find_elements_by_xpath("//p//a[@href]")


class Tools:
    def insert(self, data):
        try:
            s = self.parser_scrap(self=None, data=data, s=None)
            session.add(s)
            session.commit()
            print('add successfully !')
        except Exception as e:
            print(e.__str__())
            session.rollback()
        finally:
            session.close()

        # for elem in elems:
        #     print(elem.get_attribute("title"))
        #     print(elem.text)
        return data

    def parser_scrap(self, data, s):
        title, content = None, None
        if 'title' in data:
            title = str(data['title'])
        if 'content' in data:
            content = str(data['content'])
        if s is None:
            s = Scrap(title=title, content=content)
        else:
            s.tittle = title,
            s.content = content

        return s


class ScrapData(object):
    def main_data():
        for elem in elems:
            tit_con = {'title': elem.get_attribute("title"),
                    'content': elem.text}
            try:
                Tools.insert(self=Tools ,data=tit_con)
            except Exception as e:
              return e


run1 = ScrapData.main_data()
