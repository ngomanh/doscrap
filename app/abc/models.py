from app import Base, db


class Scrap(Base):
    __tablename__ = 'scrap'
    _id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    title = db.Column(db.String)
    content = db.Column(db.String)